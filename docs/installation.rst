============
Installation
============

At the command line::

    $ easy_install scriptqualis

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv scriptqualis
    $ pip install scriptqualis
