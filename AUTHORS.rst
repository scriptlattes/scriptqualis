=======
Credits
=======

Development Lead
----------------

* Fabio Natanael Kepler <fabio@kepler.pro.br>

Contributors
------------

* Lucas Abreu
* Cristhian W. Bilhalva
* Marcos V. Treviso

